import 'package:flutter/material.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';
import 'video.dart';

class VideoDetail extends StatelessWidget {
  @override
  final Video video;

  VideoDetail(this.video);

  Widget build(BuildContext context){
    YoutubePlayerController _controller = YoutubePlayerController(
      initialVideoId: video.videoId,
      params: YoutubePlayerParams(
        showControls: true,
        showFullscreenButton: true,
      )
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(video.title),
      ),
      body: Container(
        child: YoutubePlayerIFrame(
          controller: _controller,
          aspectRatio: 16 / 9,
        ),
      )
    );
  }
}
