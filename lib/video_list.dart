import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:google_fonts/google_fonts.dart';
import 'http_helper.dart';
import 'video_detail.dart';

class VideoList extends StatefulWidget {
  @override
  _VideoListState createState() => _VideoListState();
}

class _VideoListState extends State<VideoList> {
  late String result;
  late HttpHelper helper;
  int videosCount = 0;
  late List videos;

  void initState(){
    helper = HttpHelper();
    initialize();
    super.initState();
  }

  @override

  Future initialize() async {
    videos = <int>[];
    videos = await helper.getVideos();
    setState(() {
      videosCount = videos.length;
      videos = videos;
    });
  }

  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title: Text('GitLab Unfiltered'),),
      body: ListView.builder(
        itemCount: this.videosCount,
        itemBuilder: (BuildContext context, int position) {
          return Card(
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6),
            ),
            child: Column(
              children: [
                Stack(
                    children: [
                      Ink.image(
                        image: NetworkImage(videos[position].thumbnail),
                        height: 320,
                        fit: BoxFit.cover,
                        child: InkWell(
                          onTap: () {
                            MaterialPageRoute route = MaterialPageRoute(
                              builder: (_) => VideoDetail(videos[position])
                            );
                            Navigator.push(context, route);
                          }
                        )
                      ),
                      Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          padding: EdgeInsets.all(8),
                          color: Colors.black.withOpacity(0.7),
                          child: Column(
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  videos[position].title,
                                  style: GoogleFonts.openSans(
                                    color: Color(0xffFC6D26),
                                    fontSize: 24,
                                  )
                                )
                              ),
                              Align(
                                alignment: Alignment.bottomLeft,
                                child: Text(
                                  Jiffy(videos[position].publishedAt).format('EE, MMM do'),
                                  style: GoogleFonts.openSans(
                                    color: Color(0xffCCCCCC),
                                    fontSize: 14,
                                  ),
                                )
                              )
                            ]
                          )
                        )
                      )

                  ]
                ),
              ]
            )
          );
        }
      )
    );
  }
}
