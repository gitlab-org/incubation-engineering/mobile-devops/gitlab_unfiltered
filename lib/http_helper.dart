import 'package:http/http.dart' as http;
import 'package:flutter_config/flutter_config.dart';
import 'dart:io';
import 'dart:convert';
import 'video.dart';
final API_KEY =  FlutterConfig.get('YOU_TUBE_API_KEY');
final CHANNEL_ID = FlutterConfig.get('YOU_TUBE_CHANNEL_ID');

class HttpHelper {
  Future<List> getVideos() async {
    final String channelUrl = 'https://youtube.googleapis.com/youtube/v3/playlistItems?part=id%2C%20snippet%2C%20status&maxResults=50&playlistId=' + CHANNEL_ID + '&key=' + API_KEY;

    http.Response result = await http.get(Uri.parse(channelUrl));

    if (result.statusCode == HttpStatus.ok) {
      final jsonResponse = json.decode(result.body);
      final videosMap = jsonResponse['items'];
      List videos = videosMap.map((i) => Video.fromJson(i)).toList();
      return videos;
    } else {
      return [];
    }
  }
}
