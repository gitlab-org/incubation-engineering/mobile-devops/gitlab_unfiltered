# GitLab Unfiltered

The _easiest_ way to catch up with GitLab Unfiltered from your Android or iOS device!

<img src="https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/uploads/90686a47101db219ad0c5eec50f7f438/Screen_Shot_2021-10-13_at_2.19.15_PM.png" alt="Screen_Shot_2021-10-13_at_2.19.15_PM" width="300"/>

<img src="https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/uploads/27a99eff05498044ea6d391ebf2432f6/Screen_Shot_2021-10-13_at_2.19.27_PM.png" alt="Screen_Shot_2021-10-13_at_2.19.27_PM" width="303"/>

This app is currently available for internal testing. If you'd like to sign up as tester, please fill out this form: [https://forms.gle/A5ewcwqDonzdiQGB7](https://forms.gle/A5ewcwqDonzdiQGB7).

## Contributing

We are happy to have contributions from anyone that would like to help out. Please feel free to review, or add new issues to the project: [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/gitlab_unfiltered/-/issues](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/gitlab_unfiltered/-/issues).

## Getting Started

This app is built using [Flutter](https://flutter.dev). To get started developing on this app, please see the Flutter install guides for [Windows](https://flutter.dev/docs/get-started/install/windows), [Mac](https://flutter.dev/docs/get-started/install/macos), or [Linux](https://flutter.dev/docs/get-started/install/linux).

Once you have Flutter installed, clone this project to your machine.

#### Environment Variables

There are two env variables that will need to be set as part of the build process:

* `YOU_TUBE_API_KEY` An API key is needed to query the YouTube API for content, see YouTube API setup instructions below.
* `YOU_TUBE_CHANNEL_ID` The YouTube channel ID - [instructions to get the channel id](https://support.google.com/youtube/answer/3250431?hl=en).

#### Running the App

The app can then be run in Chrome (no emulator required) with the command below by substituting the environment variables below `SUPPLY_API_KEY` and `SUPPLY_CHANNEL_ID`.

```
flutter run --dart-define=YOU_TUBE_API_KEY=SUPPLY_API_KEY --dart-define=YOU_TUBE_CHANNEL_ID=SUPPLY_CHANNEL_ID -d chrome --web-renderer html
```

#### YouTube API setup instructions

Follow the guide here to setup API access to the YouTube API [https://developers.google.com/youtube/v3/getting-started](https://developers.google.com/youtube/v3/getting-started). OAuth is not required for this app, a simple API key credential is all that is needed.

